class Product:

    def __init__(self, id, name, price, link):
        self._id = id
        self._name = name
        self._price = price
        self._link = link

    # Getter for id
    def get_id(self):
        return self._id

    # Getter for name
    def get_name(self):
        return self._name

    # Getter for price
    def get_price(self):
        return self._price

    # Getter for link
    def get_link(self):
        return self._link
