import smtplib
import sqlite3
from email.message import EmailMessage
import scrapy

from ..spiders import database
from ..spiders.product import Product


class ItemsSpider(scrapy.Spider):
    # Spider's name
    name = "items"

    # Okidoki base url
    _base_url = 'https://www.okidoki.ee'

    # List of products that need to be sent via email
    sending_products_list = []

    def __init__(self, search_item, price, receivers_email_address):
        # Restart sending list when spider being called
        self.sending_products_list = []
        self._search_item = search_item
        self._price = price
        self._receivers_email_address = receivers_email_address
        # Make url from given search item
        self._url = make_url(self._search_item)
        # Makes db connection and adds cursor
        self._connection = sqlite3.connect('websraper.db')
        self._cursor = self._connection.cursor()
        # Creates table for db
        database.create_table(self._cursor)

    def start_requests(self):
        # HTTP request, Example: url=https://www.okidoki.ee/buy/all/?query=helly+hansen, return: response
        yield scrapy.Request(url=self._url, callback=self.parse)

    def parse(self, response, **kwargs):
        # Get all products
        products = response.css('li.classifieds__item')

        for product in products:
            # Product price initial
            product_price = None

            # Product price info: 'None', 'Tasuta' or some price
            price_info = product.css('span.horiz-offer-card__price-value::text').get()

            if price_info is None:
                product_price = 0
            else:
                # Strip excess whitespaces
                stripped_price_info = price_info.strip()
                if stripped_price_info == 'Tasuta':
                    product_price = 0
                else:
                    # Split price: price number and € sign.
                    # Get only product price number.
                    # Example: 15 000 € = 15000
                    splitted_products_price = stripped_price_info.split(" ")
                    product_price = "".join(splitted_products_price[:-1])

            # Only accept those products that meet the requirement
            if float(product_price) <= float(self._price):
                # Product url link
                product_link = self._base_url + product.css('a.horiz-offer-card__title-link::attr(href)').get()
                # Product id
                product_id = product_link.split('/')[-2]
                # Product name
                product_name = product.css('a.horiz-offer-card__title-link::text').get().strip()

                # Figure out if product can be inserted in db
                self._cursor.execute(f"SELECT * FROM product WHERE id={product_id}")
                if self._cursor.fetchone() is None:
                    # Make Product object
                    product = Product(product_id, product_name, product_price, product_link)
                    # Add Product object to list
                    self.sending_products_list.append(product)

        # Insert list of products that are not in db yet
        database.insert_to_db(self._cursor, self.sending_products_list)
        # Commits database
        self._connection.commit()

        # How many pages
        pages_number = len(response.css('li.pager__item'))
        for page_nr in range(2, pages_number):
            # Repeat parse method until no more pages
            yield scrapy.Request(url=f'{self._url}&p={page_nr}', callback=self.parse)

    def close(self, reason):
        # Call email sending function
        if len(self.sending_products_list) != 0:
            send_email(self._receivers_email_address, self.sending_products_list)


def make_url(search_item):
    # Url base with searched item
    url_with_search_item = f'https://www.okidoki.ee/buy/all/?query='

    # Split items to list. Need if searched item has spaces between
    # Example: Helly Hansen jope -> [Helly, Hansen, jope]
    splitted_search_items = search_item.split(" ")

    # Add '+' sign between words and add string to end of url base with search item
    for splitted_item in splitted_search_items:
        url_with_search_item += splitted_item
        if splitted_item != splitted_search_items[-1]:
            url_with_search_item += "+"
    return url_with_search_item


def send_email(email_address, products_list):
    # Setting up email
    msg = EmailMessage()
    msg['Subject'] = 'Found something in Okidoki.com!'
    msg['From'] = 'okidokitest1@gmail.com'
    msg['To'] = email_address

    # Collecting email content
    content = ""
    for product in products_list:
        content += f'\n{product.get_price()} € -> {product.get_link()}'

    # Set content to email
    msg.set_content(content)
    # Login to email address and sending email
    with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp:
        smtp.login('okidokitest1@gmail.com', 'testing12345??')
        smtp.send_message(msg)
