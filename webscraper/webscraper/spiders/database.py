def create_table(cursor):
    # Creating product table
    cursor.execute("CREATE TABLE IF NOT EXISTS product (id TEXT, name TEXT, price TEXT, link TEXT)")


def insert_to_db(cursor, product_list):
    # Inserting every product in list of products
    for product in product_list:
        cursor.execute(f"INSERT INTO product VALUES ('{product.get_id()}', '{product.get_name()}', "
                       f"'{product.get_price()}', '{product.get_link()}')")
