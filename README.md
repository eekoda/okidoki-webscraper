# Web scraper for Okidoki.com

App is located in okidoki_spider.py file.

App requires scrapy and itemadapter.

Use this command if you don't have these ''python -m pip install -r requirements.txt''

**Path: okidoki-webscraper/webscraper/webscraper/spiders/okidoki_spider.py**



--------------------------------------------
### How to use okidoki_spider.py?
Open command promt. Go to location, using "**cd**", where "**okidoki-webscraper\webscraper**" is located on your computer.

Example:
```
C:\Users\Kasutaja\PycharmProjects\okidoki-webscraper\webscraper>
```
After that take app activation base code and add parameters between quotation marks.

App activation base code:
```` 
scrapy crawl items -a search_item="" -a price="" -a receivers_email_address=""
````

- **search_item** - item you want to search in Okidoki.com

- **price** - maximum price product can have

- **receivers_email_address** - receiving address that gets products prices and links

After adding parameters. Example app activation code:
```` 
scrapy crawl items -a search_item="helly hansen" -a price="20" -a receivers_email_address="okidokitest1@gmail.com"
```` 
After you have changed parameters, place this code to command promt and run **okidoki_spider.py**

-------------------------------------------

### What does **okidoki_spider.py** do?
Searches Okidoki.com site with given search query and maximum price.
Products that meet the requirements will be inserted into to **websraper.db** database (location: okidoki-webscraper/webscraper/websraper.db).
Also, those products that are not in database, will be sent to given email address, formatted as {price} -> {link}.

## Note:
If you want to reset database then delete "**websraper.db**" from "**okidoki-webscraper/webscraper/websraper.db**" and run code again